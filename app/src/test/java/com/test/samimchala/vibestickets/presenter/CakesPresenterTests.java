package com.test.samimchala.vibestickets.presenter;

import com.test.samimchala.vibestickets.model.Cake;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CakesPresenterTests {

    CakesPresenterImpl presenter;
    JSONArray simpleJsonArray;

    @Before
    public void setUp(){
        presenter = new CakesPresenterImpl();
        try {
            simpleJsonArray = new JSONArray("[{\"title\":\"Lemon cheesecake\",\"desc\":\"A cheesecake made of lemon\",\"image\":\"https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg\"}]");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void parseSimpleJson() throws Exception {
        List<Cake> actualCakes = presenter.parseData(simpleJsonArray);
        assertEquals(1, actualCakes.size());
    }
}
