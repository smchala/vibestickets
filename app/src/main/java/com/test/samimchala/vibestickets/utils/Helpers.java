package com.test.samimchala.vibestickets.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public class Helpers {
    public static boolean isInternetOn(Context context) {

        boolean isConnected = false;
        ConnectivityManager connectManager =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        if (connectManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connectManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connectManager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connectManager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            isConnected = true;

        } else if (
                connectManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connectManager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
        }
        return isConnected;
    }
}