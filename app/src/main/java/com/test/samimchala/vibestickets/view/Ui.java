package com.test.samimchala.vibestickets.view;

import android.content.Context;

public interface Ui {
    Context getContext();
}