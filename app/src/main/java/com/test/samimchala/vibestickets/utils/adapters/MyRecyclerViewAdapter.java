package com.test.samimchala.vibestickets.utils.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.test.samimchala.vibestickets.R;
import com.test.samimchala.vibestickets.model.Cake;
import com.test.samimchala.vibestickets.utils.CustomVolleyRequest;

import java.util.List;


public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.CakeViewHolder> {

    List<Cake> cakes;
    private Context mContext;
    private ImageLoader imageLoader;

    public MyRecyclerViewAdapter(List<Cake> cakes, Context context) {
        this.mContext = context;
        this.cakes = cakes;
    }

    @Override
    public CakeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.
                from(mContext).inflate(R.layout.list_item_layout, parent, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        CakeViewHolder cakeViewHolder = new CakeViewHolder(rootView);
        return cakeViewHolder;
    }

    @Override
    public void onBindViewHolder(CakeViewHolder holder, int position) {
        imageLoader = CustomVolleyRequest.getInstance(mContext).getImageLoader();
        imageLoader.get(cakes.get(position).getImageUrl(), ImageLoader.getImageListener(holder.imageView, R.drawable.place_holder, android.R.drawable.ic_dialog_alert));
        holder.titleTv.setText(cakes.get(position).getTitle());
        holder.descTv.setText(cakes.get(position).getDescription());
        holder.imageView.setImageUrl(cakes.get(position).getImageUrl(), imageLoader);
    }

    @Override
    public int getItemCount() {
        if (cakes != null) {
            return cakes.size();
        }
        return 0;
    }

    @Override
    public void onViewRecycled(CakeViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CakeViewHolder extends RecyclerView.ViewHolder {
        TextView titleTv;
        TextView descTv;
        NetworkImageView imageView;

        CakeViewHolder(View itemView) {
            super(itemView);
            imageView = (NetworkImageView) itemView.findViewById(R.id.image);
            descTv = (TextView) itemView.findViewById(R.id.desc);
            titleTv = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
