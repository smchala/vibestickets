package com.test.samimchala.vibestickets.presenter;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.test.samimchala.vibestickets.model.Cake;
import com.test.samimchala.vibestickets.utils.Config;
import com.test.samimchala.vibestickets.utils.CustomVolleyRequest;
import com.test.samimchala.vibestickets.view.fragments.IPlaceHolderFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.test.samimchala.vibestickets.utils.Config.DATA_URL;
import static com.test.samimchala.vibestickets.utils.Config.TAG_DESCRIPTION;
import static com.test.samimchala.vibestickets.utils.Config.TAG_IMAGE_URL;
import static com.test.samimchala.vibestickets.utils.Config.TAG_TITLE;

public class CakesPresenterImpl extends CakesPresenter<IPlaceHolderFragment> {

    public void onUiReady(IPlaceHolderFragment ui){
        super.onUiReady(ui);
        getData(ui.getContext());
    }

    @Override
    public void onUiUnready(IPlaceHolderFragment ui){
        super.onUiUnready(ui);

        CustomVolleyRequest.getInstance(ui.getContext()).cancelPendingRequests(Config.TAG_REQUEST_OBJECT_CAKES_VIEW);
    }
    private void getData(Context context) {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(DATA_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        List<Cake> cakes = parseData(response);
                        if (cakes == null) {
                            Log.e(TAG, "requestCakes: fatal error bean is null.");
                            return;
                        }
                        getUi().getCakes(cakes);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //TODO...

                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonArrayRequest);
    }

    public List<Cake> parseData(JSONArray array){

        List<Cake> cakeList = new ArrayList<Cake>();
        for(int i = 0; i<array.length(); i++) {
            Cake cake = new Cake();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);
                cake.setImageUrl(json.getString(TAG_IMAGE_URL));
                cake.setTitle(json.getString(TAG_TITLE));
                cake.setDescription(json.getString(TAG_DESCRIPTION));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            cakeList.add(cake);
        }
        return cakeList;
    }
}
