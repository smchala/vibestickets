package com.test.samimchala.vibestickets.view.fragments;

import com.test.samimchala.vibestickets.model.Cake;
import com.test.samimchala.vibestickets.view.Ui;

import java.util.List;


public interface IPlaceHolderFragment extends Ui {
    void getCakes(List<Cake> cakes);
}
