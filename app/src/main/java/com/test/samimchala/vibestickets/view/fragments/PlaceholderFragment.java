package com.test.samimchala.vibestickets.view.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.samimchala.vibestickets.R;
import com.test.samimchala.vibestickets.model.Cake;
import com.test.samimchala.vibestickets.presenter.CakesPresenter;
import com.test.samimchala.vibestickets.presenter.CakesPresenterImpl;
import com.test.samimchala.vibestickets.utils.adapters.MyRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class PlaceholderFragment extends Fragment implements IPlaceHolderFragment{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Cake> cakeList;

    private CakesPresenter cakePresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        cakeList = new ArrayList<>();

        cakePresenter = new CakesPresenterImpl();
        cakePresenter.onUiReady(this);
    }

    @Override
    public void getCakes(List<Cake> cakes) {
        mAdapter = new MyRecyclerViewAdapter(cakes, getContext());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cakePresenter.onUiDestory(this);
    }
}