package com.test.samimchala.vibestickets.presenter;

import com.test.samimchala.vibestickets.view.Ui;


public abstract class CakesPresenter<U extends Ui> {
    private U mUi;

    public void onUiReady(U ui){
        mUi = ui;
    }

    public final void onUiDestory(U ui){
        onUiUnready(ui);
        mUi = null;
    }

    public void onUiUnready(U ui){
    }

    public U getUi(){
        return mUi;
    }
}
