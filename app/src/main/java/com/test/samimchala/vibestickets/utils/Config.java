package com.test.samimchala.vibestickets.utils;

public class Config {
    public static final String DATA_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/" +
            "raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";
    public static final String TAG_IMAGE_URL = "image";
    public static final String TAG_TITLE = "title";
    public static final String TAG_DESCRIPTION = "desc";
    public static final String TAG_REQUEST_OBJECT_CAKES_VIEW = "tag_request_object_cakes_view";
}
