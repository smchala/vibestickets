package com.test.samimchala.vibestickets;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.test.samimchala.vibestickets.view.activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.test.samimchala.vibestickets.FunctionalTestHelpers.actionOpenDrawer;
import static com.test.samimchala.vibestickets.FunctionalTestHelpers.matchToolbarTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class FunctionalTestsExample {

    @Before
    public void setUp(){}

    @Test
    public void useAppContextTest() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.test.samimchala.vibestickets", appContext.getPackageName());
    }

    @Test
    public void toolbarTitleTest() {
        CharSequence title = InstrumentationRegistry
                .getTargetContext().getString(R.string.app_name);
        matchToolbarTitle(title);
    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void checkDrawerItemsTest() {
        onView(withId(R.id.drawer_layout)).perform(actionOpenDrawer());

        onView(withText(R.string.tickets)).check(matches(withText("Tickets")));
        onView(withText(R.string.chat)).check(matches(withText("Chat")));
        onView(withText(R.string.profile)).check(matches(withText("Profile")));
    }

    @Test
    public void checkNumberOfItemsInRecyclerViewTest(){
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(20));
    }

    /*
            Does not work with the emulator however it does on a device!
     */
//    @Test
//    public void checkTicketsToastDisplayedTest() {
//        onView(withId(R.id.drawer_layout)).perform(actionOpenDrawer());
//        onView(withText(R.string.tickets)).perform(click());
//
//        onView(withText(R.string.TOAST_STRING_TICKET)).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
//    }

    @Test
    public void checkRecyclerViewFirstItemTest(){
        onView(withRecyclerView(R.id.recyclerView)
                .atPositionOnView(0, R.id.title))
                .check(matches(withText("Lemon cheesecake")));
    }

    @After
    public void tearDown(){}

    private static FunctionalTestHelpers withRecyclerView(final int recyclerViewId) {
        return new FunctionalTestHelpers(recyclerViewId);
    }

    public class RecyclerViewItemCountAssertion implements ViewAssertion {
        private final int expectedCount;

        public RecyclerViewItemCountAssertion(int expectedCount) {
            this.expectedCount = expectedCount;
        }

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();
            assertThat(adapter.getItemCount(), is(expectedCount));
        }
    }
}
