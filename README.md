I wrote a simple app, that gets a json from the cloud using volley, I parse the response to a list of cakes that holds on each a title a description and an image. support both orientation.
All images are also downloaded (lazy download) to the device for the first time then cached, there is an entry point to deal with memory issue if needed (older devices).
I have used Volley for http calls, and image loading. RecyclerView was used to populate the list.
An example of a drawer could be used to expose other routes through the app.

I have decided to use an MVP design patter as it would allow further integration to the CLEAN architecture, this setup should just represent the presentation layer, the presenter will integrate the next layer’s interface (example domain layer) to deal with the use cases (domain layer, business logic) and from there we could have a data layer which should provide the domain layer with data, regardless to where it is coming from (sockets, http, memory…)

This set up is an example that helps create an app that is modular, easy to integrate, maintain and test. Due to time constraint I had to include the comms to this presenter, it should not have any android element, breaking the principle I have just described.

There is examples of functional testing utilising espresso and unit testing

In the limited amount of time I tried to focus on the architecture (CLEAN, presentation layer), using design patterns MVP and singleton, clean code and a limited amount of tests both unit and functional.


To run the functional tests please create a new test configuration with
android.support.test.runner.AndroidJUnitRunner
as the specific instrumentation runner
Unit tests should run without any set up